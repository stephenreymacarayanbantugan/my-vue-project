  module.exports = {
    port: process.env.PORT || 8081,
    db: {
      database: process.env.DB_NAME || 'myproject',
      user: process.env.DB_USER || 'myproject',
      password: process.env.DB_PASS || 'myproject',
      options: {
        dialect: process.env.DIALECT || 'sqlite',
        host: process.env.HOST || 'localhost',
        storage: './myproject.sqlite'
      }
    }
  }
  